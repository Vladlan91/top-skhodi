<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'elion');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Pmy6PS{Qza;Si$b0^3l`2gGB=+|3!?Kkk%Vh?M4^M<4t>y>rOOd.h P`YBY>f{7');
define('SECURE_AUTH_KEY',  '*p8R`@Lmi}u{q3%5]g-GRJ_x0JkiK@WWF`SO[ewC9v.@S`?5{;<+nTWWm;SO~b^e');
define('LOGGED_IN_KEY',    'sMs1DJrPRW%0/Q[TO6t-)RA@O/1Ud<.VrF18i30frztpdV1!Wpko_KriXW@H9.%|');
define('NONCE_KEY',        'W[X-|Ah)Vigl7@b[7o[GoLVXR4>N{e;F0B#NJ0OV!5+9zZL_04[`G.; Y{3zxv]Y');
define('AUTH_SALT',        ':=#9>D;/aU`SVwO0,%HF_uNj&Dp=)Hs&h7BQ#JL3XE!jm?a/;er}e(}&#j)lcB`N');
define('SECURE_AUTH_SALT', '+AoZnmI9>kb.woJ]G(wrvL9FBuKghOn#9x4.GpZW-82s0OLK.+|,x,Q/e3AHj0=T');
define('LOGGED_IN_SALT',   ') A C:6,!pZ!)g{7Z#WD`R*y(o)F!%<hjYJ3^U:9wj;*>KNkq]qq#,yWt3=`Y+mi');
define('NONCE_SALT',       'L(SFv*H=[*<9 <X&gpU2Hi@C`@);D!`e%:>n.!w:C5(<6h[6E8~`tB;A*xOv|T I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
