<?php
qis_messages();

// Builds and manages the applications table 
function qis_messages() {
    
    if( isset( $_POST['qis_reset_message'])) {
        delete_option('qis_messages');
        qis_admin_notice(__('All applications have been deleted','quick-interest-slider').'.');
    }
    
    if( isset($_POST['qis_delete_selected'])) {
        $event = $_POST["qis_download_form"];
        $message = get_option('qis_messages');
        for($i = 0; $i <= 1000; $i++) {
            if ($_POST[$i] == 'checked') {
                unset($message[$i]);
            }
        }
        $message = array_values($message);
        update_option('qis_messages', $message );
        qis_admin_notice(__('Selected applications have been deleted','quick-interest-slider').'.');
    }

    if( isset($_POST['qis_emaillist'])) {
        $message = get_option('qis_messages');
        $content = qis_build_registration_table ($message,'report');
        $sendtoemail = $_POST['sendtoemail'];
        $headers = "From: {<{$qis_email}>\r\n"."MIME-Version: 1.0\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n";	
        wp_mail($qis_email, 'Loan Applications', $content, $headers);
        qis_admin_notice(__('Application list has been sent to','quick-interest-slider').' '.$qis_email.'.');
    }

    $content=$current=$all='';
    $message = get_option('qis_messages');
    
    global $current_user;
    get_currentuserinfo();
    
    if (!$sendtoemail) {
        $sendtoemail = $current_user->user_email;
    }

    if(!is_array($message)) $message = array();
    $dashboard = '<div class="wrap">
    <h1>'.__('Loan Applications','quick-interest-slider').'</h1>
    <div id="qis-widget">
    <form method="post" id="qis_download_form" action="">';
    $content = qis_build_registration_table ($message,'');
    if ($content) {
        $dashboard .= $content;
        $dashboard .='<p>'.__('Send to this email address','quick-interest-slider').': <input type="text" name="sendtoemail" value="'.$sendtoemail.'">&nbsp;
        <input type="submit" name="qis_emaillist" class="button-primary" value="'.__('Email List','quick-interest-slider').'" />
        <input type="submit" name="qis_reset_message" class="button-secondary" value="'.__('Delete all applications','quick-interest-slider').'" onclick="return window.confirm( \'Are you sure you want to delete all the applications?\' );"/>
        <input type="submit" name="qis_delete_selected" class="button-secondary" value="Delete Selected" onclick="return window.confirm( \'Are you sure you want to delete the selected applications?\' );"/></p>
        </form>';
    } else {
        $dashboard .= '<p>'.__('There are no applications','quick-interest-slider').'</p>';
    }
    $dashboard .= '</div></div>';		
    echo $dashboard;
}