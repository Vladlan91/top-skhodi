$ = jQuery;

var fx = {'lines':$('#currencies tbody tr').length,'addLine':function(t) {
	this.lines++;
	
	var newLine = $('#currencies tbody tr').first().clone();
	newLine.find('.input-symbol').attr({'name':'currency_array['+this.lines+'][symbol]','value':''});
	newLine.find('.input-iso').attr({'name':'currency_array['+this.lines+'][iso]','value':''});
	newLine.find('.input-name').attr({'name':'currency_array['+this.lines+'][name]','value':''});
	
	newLine.appendTo($('#currencies tbody'));
},removeLine:function(t) {
	$(t).closest('tr').remove();
}};

jQuery(document).ready(function($){
    var custom_uploader;
    $('#qis_upload_background_image').click(function(e) {
        e.preventDefault();
        if (custom_uploader) {custom_uploader.open();return;}
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Background Image',button: {text: 'Insert Image'},multiple: false});
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#qis_background_image').val(attachment.url);
        });
        custom_uploader.open();
    });
    
    $('.qis-color').wpColorPicker();
	
	$selector = $('#chkCurrency,#chkFX');
	$selector.change(function() {
		console.log($selector.is(':checked'));
		if ($selector.is(':checked')) $("#showCurrencies").show("slow");
		else {
			$("#showCurrencies").hide("slow");
		}
	});
	
	$('.fx_remove_line').click(function() {
		fx.removeLine(this);
	});
	
	$('.fx_new_line').click(function() {
		fx.addLine(this);
	});
});